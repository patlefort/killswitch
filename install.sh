#!/usr/bin/env sh

sudo cp killswitch.sh /usr/local/bin/
sudo cp killswitch-dispatcher.sh /etc/NetworkManager/dispatcher.d/pre-up.d/killswitch.sh
