#!/usr/bin/env bash

function join_by { local IFS="$1"; shift; echo "$*"; }

interface=$1 status=$2
case $status in
	vpn-pre-up)
		
		ROUTEIPS=()
		for ((i=0;i<$IP4_NUM_ROUTES;i++)); do
			ROUTEAR=( $(printenv "IP4_ROUTE_$i") )
			ROUTEIPS+=("${ROUTEAR[0]}")
		done

		killswitch.sh start "$DEVICE_IP_IFACE" $(join_by , "${ROUTEIPS[@]}") "$CONNECTION_ID" "$interface"
		;;
esac
