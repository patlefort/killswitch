Killswitch for Linux
==============================

Install by running install.sh. The killswitch will activate automatically when a VPN connection is activated through NetworkManager. To turn off the killswitch, run "sudo killswitch.sh stop". The killswitch will only allow traffic to go though the VPN, your local network or manually defined routes. It does not touch any DNS configuration.
