#!/usr/bin/env bash

chain_exists()
{
	iptables -n --list "$1" > /dev/null 2>&1
}

if [ $# -eq 0 ]; then
	echo "Usage: killswitch.sh start/stop"
	exit 0
fi

if [ $1 = "start" ]; then

	if ! chain_exists killswitch-output; then
		echo "Starting killswitch."
		
		iptables -N killswitch-ifmatch
		
		iptables -N killswitch-route_match
		iptables -A killswitch-route_match -j REJECT
		
		iptables -N killswitch-output
		iptables -A killswitch-output -j killswitch-ifmatch

		iptables -I OUTPUT -j killswitch-output
		
		ip6tables -N killswitch-output
		ip6tables -A killswitch-output -j REJECT
		
		ip6tables -I OUTPUT -j killswitch-output
		
		#nmcli con mod "$4" ipv4.ignore-auto-dns yes
		#nmcli con mod "$4" ipv6.ignore-auto-dns yes
		
		#nmcli con down "$4"
		#nmcli con up "$4"
		#chmod -w /etc/resolv.conf
	fi
	
	iptables -D killswitch-ifmatch -o "$2" -j killswitch-route_match
	iptables -I killswitch-ifmatch -o "$2" -j killswitch-route_match
	
	iptables -D killswitch-route_match -d "$3" -j RETURN
	iptables -I killswitch-route_match -d "$3" -j RETURN
	
	ip6tables -D killswitch-output -o "$5" -j RETURN
	ip6tables -I killswitch-output -o "$5" -j RETURN
	
elif [ $1 = "stop" ]; then

	if chain_exists killswitch-output; then
		echo "Stopping killswitch."
		
		iptables -F killswitch-ifmatch
		iptables -F killswitch-route_match
		iptables -F killswitch-output
		
		iptables -D OUTPUT -j killswitch-output
		
		iptables -X killswitch-ifmatch
		iptables -X killswitch-route_match
		iptables -X killswitch-output
		
		ip6tables -D OUTPUT -j killswitch-output

		ip6tables -F killswitch-output
		ip6tables -X killswitch-output
		
		#chmod u+w /etc/resolv.conf
		#nmcli con mod "$2" ipv4.ignore-auto-dns no
		#nmcli con mod "$2" ipv6.ignore-auto-dns no
		
		#nmcli con down "$2"
		#nmcli con up "$2"
		
		#systemctl restart NetworkManager.service
	fi
	
else

	echo "Invalid command $1."
	exit 1
	
fi
